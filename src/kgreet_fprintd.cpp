/*

Conversation widget for kdm greeter

Copyright (C) 1997, 1998, 2000 Steffen Hansen <hansen@kde.org>
Copyright (C) 2000-2003 Oswald Buddenhagen <ossi@kde.org>
Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#include "kgreet_fprintd.h"

#include <kglobal.h>
#include <klocale.h>
#include <klineedit.h>
#include <kuser.h>
#include <assert.h>

#include <QRegExp>
#include <QLayout>
#include <QLabel>


KFprintGreeter::KFprintGreeter( KGreeterPluginHandler *_handler,
                                  QWidget *parent,
                                  const QString &_fixedEntity,
                                  Function _func, Context _ctx ) :
	QObject(),
	KGreeterPlugin( _handler ),
	fixedUser( _fixedEntity ),
	func( _func ),
	ctx( _ctx ),
	running( false ),
	authStarted( false )
{
	QGridLayout *grid = 0;
	int line = 0;

	if (!_handler->gplugHasNode( "user-entry" ) ||
	    !_handler->gplugHasNode( "fprint-status" ) ||
	    !_handler->gplugHasNode( "anim-label" ) )
	{
		parent = new QWidget( parent );
		parent->setObjectName( "talker" );
		widgetList << parent;
		grid = new QGridLayout( parent );
		grid->setMargin( 0 );
	}
	
	loginLabel = fprintStatus = animLabel = 0;
	loginEdit = 0;
	
	if (ctx == ExUnlock)
		fixedUser = KUser().loginName();
	if (func != ChAuthTok) {
		
		timer = new QTimer();
		timer->setSingleShot(true);
		QObject::connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
		
		if (fixedUser.isEmpty()) {
			loginEdit = new KLineEdit( parent );
			loginEdit->setContextMenuPolicy( Qt::NoContextMenu );
			connect( loginEdit, SIGNAL(editingFinished()), SLOT(slotChanged()) );
			connect( loginEdit, SIGNAL(textChanged( const QString & )), SLOT(slotChanged()) );
			connect( loginEdit, SIGNAL(selectionChanged()), SLOT(slotChanged()) );
			if (!grid) {
				loginEdit->setObjectName( "user-entry" );
				widgetList << loginEdit;
			} else {
				loginLabel = new QLabel( i18n( "&Username:" ), parent );
				loginLabel->setBuddy( loginEdit );
				grid->addWidget( loginLabel, line, 0 );
				grid->addWidget( loginEdit, line++, 1 );
			}
		} else if (ctx != Login && ctx != Shutdown && ctx != ExUnlock && grid) {
			loginLabel = new QLabel( i18n( "Username:" ), parent );
			grid->addWidget( loginLabel, line, 0 );
			grid->addWidget( new QLabel( fixedUser, parent ), line++, 1 );
		}
		
		fprintStatus = new QLabel();
		if (!grid) {
			fprintStatus->setObjectName( "fprint-status" );
			widgetList << fprintStatus;
		} else{
			grid->addWidget( fprintStatus, line++, 0, 1, 2 );
		}

		animLabel = new SensorAnimLabel();
		if (!grid) {
			animLabel->setObjectName( "anim-label" );
			widgetList << animLabel;
		} else {
			grid->addWidget( animLabel, 0, 2, 2, 1 );
		}
		
		if (loginEdit) {
			loginEdit->setFocus();
		}
		
		if (ctx == ExUnlock) {
			fprintStatus->setText( i18n( "Click on Unlock button." ) );
		}
	}
}

void KFprintGreeter::showText() {
	if ( authStarted ) {
		QString text;
		if (sensor == i18n("swipe")) {
			text = QString(i18n("Swipe your %1"));
		} else {
			text = QString(i18n("Place your %1"));
		}
		text = text.arg(i18n(finger.toLatin1()));
		animLabel->start();
		fprintStatus->setText( text );
	}
}

void KFprintGreeter::timeout() {
	if ( running && animLabel ) {
		showText();
	}
}

// virtual
KFprintGreeter::~KFprintGreeter()
{
	abort();
	qDeleteAll( widgetList );
}

void // virtual
KFprintGreeter::loadUsers( const QStringList &users )
{
	KCompletion *userNamesCompletion = new KCompletion;
	userNamesCompletion->setItems( users );
	loginEdit->setCompletionObject( userNamesCompletion );
	loginEdit->setAutoDeleteCompletionObject( true );
	loginEdit->setCompletionMode( KGlobalSettings::CompletionAuto );
}

void // virtual
KFprintGreeter::presetEntity( const QString &entity, int field )
{
	loginEdit->setText( entity );
	if (field == 0) {
		loginEdit->setFocus();
		loginEdit->selectAll();
	}
	curUser = entity;
}

QString // virtual
KFprintGreeter::getEntity() const
{
	return fixedUser.isEmpty() ? loginEdit->text() : fixedUser;
}

void // virtual
KFprintGreeter::setUser( const QString &user )
{
	// assert( fixedUser.isEmpty() );
	curUser = user;
	loginEdit->setText( user );
}

void // virtual
KFprintGreeter::setEnabled( bool enable )
{
	// assert( func == Authenticate && ctx == Shutdown );
	if (loginLabel) {
		loginLabel->setEnabled( enable );
		loginEdit->setEnabled( enable );
	}
	
	fprintStatus->setEnabled( enable );
	
	setActive( enable );
}

void // private
KFprintGreeter::returnData()
{
	handler->gplugReturnText( (loginEdit ? loginEdit->text() :
			fixedUser).toLocal8Bit(),
			KGreeterPluginHandler::IsUser );
}

bool // virtual
KFprintGreeter::textMessage( const char *text, bool err )
{
	QString msg = QString(text);
	
	if ( err )
	{
		// handle known error messages like
		//   Place your finger on the reader again
		//   Swipe your finger again
		//   Swipe was too short, try again
		//   Your finger was not centered, try swiping your finger again
		//   Remove your finger, and try swiping your finger again
		//   Failed to match fingerprint
		if ( msg.indexOf( "Place your finger on the reader again" ) > -1 ||
		     msg.indexOf( "Swipe your finger again" ) > -1 ||
		     msg.indexOf( "Swipe was too short, try again" ) > -1 ||
		     msg.indexOf( "Your finger was not centered, try swiping your finger again" ) > -1 ||
		     msg.indexOf( "Remove your finger, and try swiping your finger again" ) > -1 ||
		     msg.indexOf( "Failed to match fingerprint" ) > -1 )
		{
			animLabel->stop();
			timer->start(2500);
			fprintStatus->setText( i18n(msg.toLatin1()) );
// 			QApplication::processEvents(); //force UI repaint, UI is locked somewhere...
			return true;
		}
	} else { // !err
		// handle know info messages like
		//   ZZZ your XXX on YYY
		// ZZZ sensor type (swipe/place)
		// XXX finger name
		// YYY device name
		// Verification timed out
		QRegExp re = QRegExp( i18n( "(.*) your (.*) on (.*)" ) );
		// this information should be used for right animation
		if (msg.indexOf(re) > -1)
		{
			sensor = re.cap(1).toLower();
			finger = re.cap(2).toLower();
			animLabel->loadMovie(sensor);
			showText();
			return true;
		} else if (msg == i18n("Verification timed out")) {
			fprintStatus->setText( i18n(msg.toLatin1()) );
			return true;
		}
	}
	return false;
}

void // virtual
KFprintGreeter::textPrompt( const char *prompt, bool echo, bool nonBlocking )
{
	Q_UNUSED(echo);
	Q_UNUSED(nonBlocking);

	QString pr = QString(prompt);
	
	if (pr != "Username:") {
		fprintStatus->setText( pr );
	}
// 	QApplication::processEvents();
}

bool // virtual
KFprintGreeter::binaryPrompt( const char *, bool )
{
	// this simply cannot happen ... :}
	return true;
}

void // virtual
KFprintGreeter::start()
{
	running = true;
}

void // virtual
KFprintGreeter::suspend()
{
}

void // virtual
KFprintGreeter::resume()
{
}

void // virtual
KFprintGreeter::next()
{
	// assert( running );
	setActive(false);
	authStarted = true;
	handler->gplugStart();
	returnData();
}

void // virtual
KFprintGreeter::abort()
{
	if ( authStarted ) {
		handler->gplugReturnText( 0, 0 );
	}
	animLabel->stop();
	fprintStatus->clear();
	authStarted = false;
	running = false;
}

void // virtual
KFprintGreeter::succeeded()
{
	// assert( running || timed_login );
	if (!authTok) {
		setActive( false );
	}
	running = false;
	authStarted = false;
	animLabel->stop();
	fprintStatus->setText(i18n("Succeeded"));
}

void // virtual
KFprintGreeter::failed()
{
	// assert( running || timed_login );
	setActive( false );
 	fprintStatus->clear();
	running = false;
	authStarted = false;
	animLabel->stop();
}

void // virtual
KFprintGreeter::revive()
{
	// assert( !running );
	// assert( authTok );
	
	setActive( true );
	loginEdit->setFocus();
}

void // virtual
KFprintGreeter::clear()
{
	if (loginEdit) {
		loginEdit->clear();
		loginEdit->setFocus();
		curUser.clear();
	}
	fprintStatus->clear();
// 	QApplication::processEvents();
}


// private

void
KFprintGreeter::setActive( bool enable )
{
	if (loginEdit)
		loginEdit->setEnabled( enable );
	//fprintStatus->setEnabled( enable );
}

void
KFprintGreeter::slotChanged()
{
	if ( running ) {
		loginEdit->setText( loginEdit->text().trimmed() );
		
		if (curUser != loginEdit->text()) {
			curUser = loginEdit->text();
			handler->gplugSetUser( curUser );
		}
		
		handler->gplugChanged();
	}
}

// factory

static bool init( const QString &,
                  QVariant (*getConf)( void *, const char *, const QVariant & ),
                  void *ctx )
{
	Q_UNUSED(getConf);
	Q_UNUSED(ctx);
	KGlobal::locale()->insertCatalog( "kgreet_fprintd" );
	return true;
}

static void done( void )
{
	KGlobal::locale()->removeCatalog( "kgreet_fprintd" );
}

static KGreeterPlugin *
create( KGreeterPluginHandler *handler,
        QWidget *parent,
        const QString &fixedEntity,
        KGreeterPlugin::Function func,
        KGreeterPlugin::Context ctx )
{
	return new KFprintGreeter( handler, parent, fixedEntity, func, ctx );
}

KDE_EXPORT KGreeterPluginInfo kgreeterplugin_info = {
	I18N_NOOP2("@item:inmenu authentication method", "Username + Fingerprint"), "fprintd",
	KGreeterPluginInfo::Local | KGreeterPluginInfo::Presettable,
	init, done, create
};

#include "kgreet_fprintd.moc"
