#include <KStandardDirs>
#include <KLocale>

#include "SensorAnimLabel.h"

/**
 * SensorAnimLabel - label with swipe/place animation.
 * @param sensor sensor type for selecting right animation
 * @param parent parent widget
 */
SensorAnimLabel::SensorAnimLabel(QWidget *parent) : QLabel(parent) {
	sensor = i18n("swipe");
	movie = new QMovie(KStandardDirs::locate("data", "kgreet_fprintd/pics/swipe.gif"));

	movie->start();
	movie->setPaused(true);
	setMovie(movie);
	setFixedSize(minimumSizeHint());
	clear();
}

void SensorAnimLabel::loadMovie(QString s) {
	if (sensor == s) {
		return;
	} else {
		sensor = s;
	}

	stop();
	if (movie) delete movie;
	
	if (s == i18n("swipe")) {
		movie = new QMovie(KStandardDirs::locate("data", "kgreet_fprintd/pics/swipe.gif"));
	} else {
		// TODO get some animation for non-swipe sensors
		movie = new QMovie(KStandardDirs::locate("data", "kgreet_fprintd/pics/swipe.gif"));
	}

	movie->start();
	movie->setPaused(true);
	setMovie(movie);
}

/**
 * SensorAnimLabel destructor.
 */
SensorAnimLabel::~SensorAnimLabel() {
	movie->stop();
	clear();
	delete(movie);
}

/**
 * start - start animation playback.
 */
void SensorAnimLabel::start() {
	setMovie(movie);
	movie->setPaused(false);
}

/**
 * stop - stops animation playback
 */
void SensorAnimLabel::stop() {
	movie->setPaused(true);
	clear();
}
