#ifndef ENROLL_ANIM_LABEL_
#define ENROLL_ANIM_LABEL_

#include <QLabel>
#include <QWidget>
#include <QMovie>

class SensorAnimLabel : public QLabel {
	Q_OBJECT

	private:
		QMovie *movie;
		QString sensor;
	public:
		SensorAnimLabel(QWidget *parent = 0);
		~SensorAnimLabel();
		void start();
		void stop();
		void loadMovie(QString sensor);
};

#endif