/*

Conversation widget for kdm greeter

Copyright (C) 1997, 1998 Steffen Hansen <hansen@kde.org>
Copyright (C) 2000-2003 Oswald Buddenhagen <ossi@kde.org>
Copyright (C) 2009 Jaroslav Barton <djaara@djaara.net>


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/


#ifndef KGREET_FPRINT_H
#define KGREET_FPRINT_H

#include <kgreeterplugin.h>

#include <QObject>
#include <QGridLayout>
#include <QTimer>

#include "SensorAnimLabel.h"

class KLineEdit;
class KSimpleConfig;
class QLabel;

class KFprintGreeter : public QObject, public KGreeterPlugin {
	Q_OBJECT

  public:
	KFprintGreeter( KGreeterPluginHandler *handler,
	                 QWidget *parent,
	                 const QString &fixedEntitiy,
	                 Function func, Context ctx );
	~KFprintGreeter();
	virtual void loadUsers( const QStringList &users );
	virtual void presetEntity( const QString &entity, int field );
	virtual QString getEntity() const;
	virtual void setUser( const QString &user );
	virtual void setEnabled( bool on );
	virtual bool textMessage( const char *message, bool error );
	virtual void textPrompt( const char *prompt, bool echo, bool nonBlocking );
	virtual bool binaryPrompt( const char *prompt, bool nonBlocking );
	virtual void start();
	virtual void suspend();
	virtual void resume();
	virtual void next();
	virtual void abort();
	virtual void succeeded();
	virtual void failed();
	virtual void revive();
	virtual void clear();

  public Q_SLOTS:
	void slotChanged();
	void timeout();

  private:
	void setActive( bool enable );
	void returnData();
	void showText();
	QLabel *loginLabel, *fprintStatus;
	QTimer *timer;
	QString sensor, finger;
	SensorAnimLabel *animLabel;
	KLineEdit *loginEdit;
	KSimpleConfig *stsFile;
	QString fixedUser, curUser;
	Function func;
	Context ctx;
	int has;
	bool running, authTok, authStarted;
};

#endif /* KGREET_FPRINT_H */
